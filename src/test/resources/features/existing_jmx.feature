Feature: Existing jmx file
COMO un usuario del microservicio PersonParametersSvc
QUIERO ejecutar peticiones a diferentes endpoint que publica
PARA verificar que el mismo responda apropiadamente.

@Nubi1 @Authorization1
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |300	  |300 		    |nubi_global_processing_authorization               |nubi_global_processing_authorization	                        |

@Nubi2 @Authorization2
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |600		|300			    |nubi_global_processing_authorization               |nubi_global_processing_authorization	                        |

@Nubi4 @Authorization4
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	|1200		|300			    |nubi_global_processing_authorization               |nubi_global_processing_authorization	                        |

@Nubi3 @Authorization3
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |900		|300			    |nubi_global_processing_authorization               |nubi_global_processing_authorization	                        |

  
@Nubi5 @Authorization5
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |1500		|300			    |nubi_global_processing_authorization               |nubi_global_processing_authorization	                        |

@Nubi6 @Authorization6
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |1800		|300			    |nubi_global_processing_authorization               |nubi_global_processing_authorization	                        |

@Nubi7 @Authorization7
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  
@Nubi1 @CancelAuthorization1
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |300		  |300 		    |nubi_global_processing_cancel_authorization               |nubi_global_processing_authorization	                        |

@Nubi2 @CancelAuthorization2
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |600		|300			    |nubi_global_processing_cancel_authorization               |nubi_global_processing_authorization	                        |

@Nubi5 @CancelAuthorization5
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |1800		|300			    |nubi_global_processing_cancel_authorization               |nubi_global_processing_authorization	                        |

@Nubi1 @RejectedAuthorization1
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		    	  |300		  |300 		    |nubi_global_processing_rejected_authorization               |nubi_global_processing_authorization	                        |

@Nubi2 @RejectedAuthorization2
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |600		|300			    |nubi_global_processing_rejected_authorization               |nubi_global_processing_authorization	                        |

@Nubi5 @RejectedAuthorization5
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |1800		|300			    |nubi_global_processing_rejected_authorization               |nubi_global_processing_authorization	                        |

@Nubi1 @Cancel1
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |300		  |300 		    |nubi_global_processing_cancel               |nubi_global_processing_authorization	                        |

@Nubi2 @Cancel2
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		    	  |600		|300			    |nubi_global_processing_cancel               |nubi_global_processing_authorization	                        |

@Nubi5 @Cancel5
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		    	  |1800		|300			    |nubi_global_processing_cancel               |nubi_global_processing_authorization	                        |

@Nubi1 @RejectedWithDraw1
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1           |300		  |300 		    |nubi_global_processing_rejected_withdraw               |nubi_global_processing_authorization	                        |

@Nubi2 @RejectedWithDraw2
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |600		|300			    |nubi_global_processing_rejected_withdraw               |nubi_global_processing_authorization	                        |

@Nubi5 @RejectedWithDraw5
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |1800		|300			    |nubi_global_processing_rejected_withdraw               |nubi_global_processing_authorization	                        |

@Nubi1 @Balance1
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		    	  |300		  |300 		    |nubi_global_processing_balance               |nubi_global_processing_authorization	                        |

@Nubi2 @Balance2
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	  |600		|300			    |nubi_global_processing_balance               |nubi_global_processing_authorization	                        |

@Nubi5 @Balance5
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |1800		|300			    |nubi_global_processing_balance               |nubi_global_processing_authorization	                        |

 @Nubi1 @CancelWithDraw1
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |300		  |300 		    |nubi_global_processing_cancel_withdraw               |nubi_global_processing_authorization	                        |

@Nubi2 @CancelWithDraw2
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |600		|300			    |nubi_global_processing_cancel_withdraw               |nubi_global_processing_authorization	                        |

@Nubi5 @CancelWithDraw5
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |1800		|300			    |nubi_global_processing_cancel_withdraw               |nubi_global_processing_authorization	                        |

@Nubi1 @WithDraw1
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |300		  |300 		    |nubi_global_processing_withdraw               |nubi_global_processing_authorization	                        |

@Nubi2 @WithDraw2
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |600		|300			    |nubi_global_processing_withdraw               |nubi_global_processing_authorization	                        |

@Nubi5 @WithDraw5
  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use static data
	And remplace variables with '<json>'
	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |1800		|300			    |nubi_global_processing_withdraw               |nubi_global_processing_authorization	                        |



 # | 10			    |120		|60			    |nubi_global_processing_cancel_authorization        |nubi_global_processing_authorization	                        |
 # | 10			    |120		|60		    	|nubi_global_processing_rejected_authorization      |nubi_global_processing_authorization	                        |
 # | 10			    |120		|60			    |nubi_global_processing_cancel                      |nubi_global_processing_authorization	                        |
 # | 10			    |120		|60		    	|nubi_global_processing_rejected_withdraw           |nubi_global_processing_authorization	                        |
 # | 10			    |120		|60		    	|nubi_global_processing_balance                     |nubi_global_processing_authorization	                        |
 # | 10			    |120		|60		    	|nubi_global_processing_cancel_withdraw             |nubi_global_processing_authorization	                        |
 # | 10			    |120		|60		    	|nubi_global_processing_withdraw                    |nubi_global_processing_authorization	                        |
