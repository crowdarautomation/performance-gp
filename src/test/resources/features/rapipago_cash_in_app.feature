@Performance @App @Rapipago @CashIn
Feature: Existing jmx file
COMO un usuario de la app
QUIERO ejecutar peticiones a Rapipago
PARA verificar que el mismo responda apropiadamente.

  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And use generated data method 'rapipagoData'
	And remplace variables with '<json>'
	
  @1ps	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |300		  |300 		    |nubi_app_rapipago_cashin               |nubi_app_rapipago_cashin	                        |

  @2ps
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |600		|300			    |nubi_app_rapipago_cashin               |nubi_app_rapipago_cashin	                        |

  @3ps
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |900		|300			    |nubi_app_rapipago_cashin               |nubi_app_rapipago_cashin	                        |

  @4ps
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |1200		|300			    |nubi_app_rapipago_cashin               |nubi_app_rapipago_cashin	                        |
  
  @5ps
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |1500		|300			    |nubi_app_rapipago_cashin               |nubi_app_rapipago_cashin	                        |
  
  @6ps
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |1800		|300			    |nubi_app_rapipago_cashin               |nubi_app_rapipago_cashin	                        |
  
  @7ps
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |2100		|300			    |nubi_app_rapipago_cashin               |nubi_app_rapipago_cashin	                        |
  
  @8ps
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |2400		|300			    |nubi_app_rapipago_cashin               |nubi_app_rapipago_cashin	                        |
  
  @9ps
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |2700		|300			    |nubi_app_rapipago_cashin               |nubi_app_rapipago_cashin	                        |
  
  @10ps
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |3000		|300			    |nubi_app_rapipago_cashin               |nubi_app_rapipago_cashin	                        |
  
