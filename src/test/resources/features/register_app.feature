@Performance @App @Register
Feature: Existing jmx file
COMO un usuario de la app
QUIERO ejecutar peticiones a register
PARA verificar que el mismo responda apropiadamente.

  Scenario Outline: Run performance with existing jmx file
	Given configure the performance test with '<iterations>' iterations '<users>' users and a ramp time of '<ramp_time>' seconds
	When run the script '<script_name>'
	And remplace variables with '<json>'
	
  @1ps	
  Examples:
  | iterations	|users	|ramp_time	|script_name						                            |json								                                          |
  | 1		  	    |300		  |300 		    |0_nubi_app_register               |nubi_app_register	                        |

  @2ps
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |600		|300			    |0_nubi_app_register               |nubi_app_register	                        |

  @3ps
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |900		|300			    |0_nubi_app_register               |nubi_app_register	                        |

  @4ps
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |1200		|300			    |0_nubi_app_register               |nubi_app_register	                        |
  
  @5ps
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |1500		|300			    |0_nubi_app_register               |nubi_app_register	                        |
  
  @6ps
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |1800		|300			    |0_nubi_app_register               |nubi_app_register	                        |
  
  @7ps
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |2100		|300			    |0_nubi_app_register               |nubi_app_register	                        |
  
  @8ps
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |2400		|300			    |0_nubi_app_register               |nubi_app_register	                        |
  
  @9ps
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |2700		|300			    |0_nubi_app_register               |nubi_app_register	                        |
  
  @10ps
  Examples:
  | iterations	|users	|ramp_time	|script_name						                       |json								                                          |
  | 1		    	  |3000		|300			    |0_nubi_app_register               |nubi_app_register	                        |
  
