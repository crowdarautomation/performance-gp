package com.crowdar.performance;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import com.crowdar.core.PropertyManager;
import com.crowdar.performance.model.CsvFileGeneratorConfig;
import com.crowdar.performance.utils.DatabaseUtils;
import com.crowdar.performance.utils.FakeDataGenerator;
import com.crowdar.performance.utils.FileUtils;
import com.crowdar.performance.utils.Queries;

public class GenerateData {

	private static final String QUERY_GET_PERSONS = "SELECT TOP %s BTDocuNume, BTEntiNacioCodi, BTDocuTipoCodi   FROM SociosComple sc (nolock) WHERE sc.FlagEmpresa=%s ORDER BY RAND(CHECKSUM(*) * RAND());";
	private static final String QUERY_GET_STOP_DEBIT = "SELECT  TOP %s Cuenta, EntiNume, MarcaCodi FROM SociosComple sc (nolock) WHERE sc.FlagEmpresa= %s AND estadoCodi = 1 ORDER BY RAND(CHECKSUM(*) * RAND());";
	private static final String TOP_REPLACEMENT = "SELECT  TOP %s ";
	private static final String QUERY_GET_USER_P2P = "SELECT '0000' AS pin,username AS username,id AS id,email AS email FROM public.\"user\" WHERE username LIKE '%s' AND modified_at IS NULL AND last_name = 'PERFORMANCE' ORDER BY random() LIMIT %s";
	private static final String QUERY_GET_ACCOUNT_P2P = "SELECT \"number\" AS account_number,user_id AS user_id FROM public.accounts WHERE user_id IN ('%s')";
	private static final String QUERY_GET_PATH = "SELECT CASE WHEN status = 'OK' THEN 'apply' WHEN status = 'REJECTED' THEN 'reject' WHEN status = 'CANCELLED' THEN 'cancel' END AS status FROM public.p2p_requests WHERE status IN ('OK','REJECTED','CANCELLED') GROUP BY status ORDER BY random() LIMIT 1";
	
	
	/**
	 * Creates a csv file used by the jmeter to create various types of credit account with random dni
	 *
	 * @param 	pathFile
	 * 		  	to store the generated csv, relative to the project folder root location
	 * @param 	cantidadGenerar
	 * 		  	total number of data rows
	 *
	 * @throws 	Exception
	 * 			if the generator function fails
	 */
	public static void generateCuentasCreditoDataRandom(String pathFile, int cantidadGenerar) throws Exception {
		File file = new File(Paths.get(pathFile).toAbsolutePath().toString());
		if (file.exists()) {
			file.delete();
		}
		file.createNewFile();

		BufferedWriter writer = Files.newBufferedWriter(Paths.get(pathFile));
		CSVPrinter csvPrinter = new CSVPrinter(writer,
				CSVFormat.INFORMIX_UNLOAD.withHeader("nro_documento", "pais", "tipo").withDelimiter(','));

		for (int i = 0; i < cantidadGenerar; i++) {
			csvPrinter.printRecord(FakeDataGenerator.generateRandomDni(),"80","6");
		}
		csvPrinter.flush();
		csvPrinter.close();
	}
	
	public static void generatep2pData(String pathFile, int cantidadGenerar) throws Exception {
		File file = new File(Paths.get(pathFile).toAbsolutePath().toString());
		if (file.exists()) {
			file.delete();
		}
		file.createNewFile();
		
		String query_user = String.format(QUERY_GET_USER_P2P, "jmeter%",Integer.toString(cantidadGenerar + 1) );
		
		BufferedWriter writer = Files.newBufferedWriter(Paths.get(pathFile));
		CSVPrinter csvPrinter = new CSVPrinter(writer,
				CSVFormat.INFORMIX_UNLOAD.withHeader("PIN","F_USER","ACCOUNT","EMAIL","ID","S_USER","F_ID","PATH").withDelimiter(','));

		List<Map<String, Object>> data = DatabaseUtils.executeQueryWithHost(query_user,"users");		  
		Map<String, Object> lastPerson = null;
		for (Map<String, Object> person : data) {
			if(lastPerson == null) {
				String query_account = String.format(QUERY_GET_ACCOUNT_P2P,person.get("id"));
				List<Map<String, Object>> useri_id = DatabaseUtils.executeQueryWithHost(query_account,"accounts");	
				if(useri_id.size() == 0) {
					continue;
				}
				person.put("user_id", useri_id.get(0).get("user_id"));
				
				lastPerson = person;
			}else {		
				
				String query_account = String.format(QUERY_GET_ACCOUNT_P2P,person.get("id"));
				List<Map<String, Object>> account = DatabaseUtils.executeQueryWithHost(query_account,"accounts");	
				
				if(account.size() == 0) {
					continue;
				}
				
				String query_path = String.format(QUERY_GET_PATH,person.get("status"));
				List<Map<String, Object>> status = DatabaseUtils.executeQueryWithHost(query_path,"p2p");
				
				csvPrinter.printRecord(person.get("pin"),person.get("username"),account.get(0).get("account_number"),person.get("email"),lastPerson.get("user_id"),lastPerson.get("username"),person.get("id"),status.get(0).get("status"));

				person.put("account_number", account.get(0).get("account_number"));
				lastPerson = null;
				//lastPerson = person;
			}
		}
		
		csvPrinter.flush();
		csvPrinter.close();
	}
	
	
	public static void generateRapipagoData(String pathFile, int cantidadGenerar, String pathFileCellPhone) throws Exception {		
		File file = new File(Paths.get(pathFile).toAbsolutePath().toString());
		if (file.exists()) {
			file.delete();
		}
		file.createNewFile();
		
		String query_user = String.format(QUERY_GET_USER_P2P, "jmeter%",Integer.toString(cantidadGenerar) );
				
		List<String> cellphones = FileUtils.getLines(pathFileCellPhone,false);
		
		BufferedWriter writer = Files.newBufferedWriter(Paths.get(pathFile));
		CSVPrinter csvPrinter = new CSVPrinter(writer,
				CSVFormat.INFORMIX_UNLOAD.withHeader("PIN","D_USER","ACCOUNT_NUMBER","PHONE").withDelimiter(','));

		List<Map<String, Object>> data = DatabaseUtils.executeQueryWithHost(query_user,"users");		  
		for (Map<String, Object> person : data) {
						
				Random rand = new Random();
			    
				int randomIndex = rand.nextInt(cellphones.size());
			    String randomElement = cellphones.get(randomIndex);
			    
				String query_account = String.format(QUERY_GET_ACCOUNT_P2P,person.get("id"));
				List<Map<String, Object>> account = DatabaseUtils.executeQueryWithHost(query_account,"accounts");	
				
				String query_path = String.format(QUERY_GET_PATH,person.get("status"));
				List<Map<String, Object>> status = DatabaseUtils.executeQueryWithHost(query_path,"p2p");
				
				csvPrinter.printRecord(person.get("pin"),person.get("username"),account.get(0).get("account_number"),randomElement);

		}
		
		csvPrinter.flush();
		csvPrinter.close();
	}
	
	/**
	 * Creates a csv file used by the jmeter to create various types of credit account with random dni
	 *
	 * @param 	pathFile
	 * 		  	to store the generated csv, relative to the project folder root location
	 * @param 	cantidadGenerar
	 * 		  	total number of data rows
	 * @param 	percentageBusiness
	 * 		  	Porcentaje de datos que sera del tipo empresa
	 *
	 * @throws 	Exception
	 * 			if the generator function fails
	 */
	public static void generateCuentasCreditoDataFromDatabase(String pathFile, int cantidadGenerar, int percentageBusiness) throws Exception {
		File file = new File(Paths.get(pathFile).toAbsolutePath().toString());
		if (file.exists()) {
			file.delete();
		}
		file.createNewFile();
		int cuantityBusiness = cantidadGenerar * percentageBusiness / 100;
		String query = String.format(QUERY_GET_PERSONS, Integer.toString(cuantityBusiness), "1");

		BufferedWriter writer = Files.newBufferedWriter(Paths.get(pathFile));
		CSVPrinter csvPrinter = new CSVPrinter(writer,
				CSVFormat.INFORMIX_UNLOAD.withHeader("nro_documento", "pais", "tipo").withDelimiter(','));

		List<Map<String, Object>> data = DatabaseUtils.executeQuery(query );
		for (Map<String, Object> person : data) {
			csvPrinter.printRecord(person.get("BTDocuNume"),person.get("BTEntiNacioCodi"),person.get("BTDocuTipoCodi"));
		}
		
		query = String.format(QUERY_GET_PERSONS, Integer.toString(cantidadGenerar-cuantityBusiness), "0");
		data = DatabaseUtils.executeQuery(query );
		for (Map<String, Object> person : data) {
			csvPrinter.printRecord(person.get("BTDocuNume"),person.get("BTEntiNacioCodi"),person.get("BTDocuTipoCodi"));
		}
		csvPrinter.flush();
		csvPrinter.close();
	}
	
	/**
	 * Creates a csv file used by the jmeter to create various types of credit account to stop debit
	 *
	 * @param 	pathFile
	 * 		  	to store the generated csv, relative to the project folder root location
	 * @param 	cantidadGenerar
	 * 		  	total number of data rows
	 * @param 	percentageBusiness
	 * 		  	Porcentaje de datos que sera del tipo empresa
	 *
	 * @throws 	Exception
	 * 			if the generator function fails
	 */
	public static void generateCuentasCreditoDataToStopDebitFromDatabase(String pathFile, int cantidadGenerar, int percentageBusiness) throws Exception {
		File file = new File(Paths.get(pathFile).toAbsolutePath().toString());
		if (file.exists()) {
			file.delete();
		}
		file.createNewFile();
		int cuantityBusiness = cantidadGenerar * percentageBusiness / 100;
		String query = String.format(QUERY_GET_STOP_DEBIT, Integer.toString(cuantityBusiness), "1");
		
		BufferedWriter writer = Files.newBufferedWriter(Paths.get(pathFile));
		CSVPrinter csvPrinter = new CSVPrinter(writer,
				CSVFormat.INFORMIX_UNLOAD.withHeader("Cuenta", "EntiNume", "MarcaCodi").withDelimiter(','));

		List<Map<String, Object>> data = DatabaseUtils.executeQuery(query );
		for (Map<String, Object> person : data) {
			csvPrinter.printRecord(person.get("Cuenta"),person.get("EntiNume"),person.get("MarcaCodi"));
		}
		
		query = String.format(QUERY_GET_STOP_DEBIT, Integer.toString(cantidadGenerar-cuantityBusiness), "0");
		data = DatabaseUtils.executeQuery(query );
		for (Map<String, Object> person : data) {
			csvPrinter.printRecord(person.get("Cuenta"),person.get("EntiNume"),person.get("MarcaCodi"));
		}
		csvPrinter.flush();
		csvPrinter.close();
	}
	
	/**
	 * Creates a csv file used by the jmeter to create person types of credit account with random dni
	 *
	 * @param 	pathFile
	 * 		  	to store the generated csv, relative to the project folder root location
	 * @param 	cantidadGenerar
	 * 		  	total number of data rows
	 *
	 * @throws 	Exception
	 * 			if the generator function fails
	 */
	public static void generateCuentasCreditoPersonDataFromDatabase(String pathFile, int cantidadGenerar) throws Exception {
		generateCuentasCreditoDataFromDatabase(pathFile, cantidadGenerar, 0);
	}
	
	/**
	 * Creates a csv file used by the jmeter to create Business types of credit account with random dni
	 *
	 * @param 	pathFile
	 * 		  	to store the generated csv, relative to the project folder root location
	 * @param 	cantidadGenerar
	 * 		  	total number of data rows
	 *
	 * @throws 	Exception
	 * 			if the generator function fails
	 */
	public static void generateCuentasCreditoBusinessDataFromDatabase(String pathFile, int cantidadGenerar) throws Exception {
		generateCuentasCreditoDataFromDatabase(pathFile, cantidadGenerar, 100);
	}
	
	/**
	 * Creates a csv file used by the jmeter to create person types of credit account to stop debit
	 *
	 * @param 	pathFile
	 * 		  	to store the generated csv, relative to the project folder root location
	 * @param 	cantidadGenerar
	 * 		  	total number of data rows
	 *
	 * @throws 	Exception
	 * 			if the generator function fails
	 */
	public static void generateCuentasCreditoPersonDataToStopDebitFromDatabase(String pathFile, int cantidadGenerar) throws Exception {
		generateCuentasCreditoDataToStopDebitFromDatabase(pathFile, cantidadGenerar, 0);
	}
	
	/**
	 * Creates a csv file used by the jmeter to create percentage random types of credit account to stop debit
	 *
	 * @param 	pathFile
	 * 		  	to store the generated csv, relative to the project folder root location
	 * @param 	cantidadGenerar
	 * 		  	total number of data rows
	 *
	 * @throws 	Exception
	 * 			if the generator function fails
	 */
	public static void generateCuentasCreditoBusinessDataToStopDebitFromDatabase(String pathFile, int cantidadGenerar) throws Exception {
		generateCuentasCreditoDataToStopDebitFromDatabase(pathFile, cantidadGenerar, 100);
	}
	
	/**
	 * Creates a csv file used by the jmeter to create Business types of credit account to stop debit
	 *
	 * @param 	pathFile
	 * 		  	to store the generated csv, relative to the project folder root location
	 * @param 	cantidadGenerar
	 * 		  	total number of data rows
	 *
	 * @throws 	Exception
	 * 			if the generator function fails
	 */
	public static void generateCuentasCreditoRandomPercentageDataToStopDebitFromDatabase(String pathFile, int cantidadGenerar) throws Exception {
		Random r = new Random();
		generateCuentasCreditoDataToStopDebitFromDatabase(pathFile, cantidadGenerar, r.nextInt(100));
	}
	
	public static String generateCorrectionFile(CsvFileGeneratorConfig config) throws Exception {
		SimpleDateFormat date = new SimpleDateFormat("yyyyMMdd");
		Date now = new Date();
		String time = new SimpleDateFormat("HHmmss").format(now);

		String zipName = String.format("_%s_ITXC.zip");
//		GenerateInterCSCFiles.writeToZipFile("src/test/jmeter/",filename, zipName);

//        GenerateInterCSCFiles.uploadToFtp("src/test/jmeter/"+zipName,zipName, config);

		return zipName;
	}
	
	/**
	 * Creates a csv file used by the jmeter to create various types of credit account to stop debit
	 *
	 * @param 	pathFile
	 * 		  	to store the generated csv, relative to the project folder root location
	 * @param 	cantidadGenerar
	 * 		  	total number of data rows
	 * @param 	percentageBusiness
	 * 		  	Porcentaje de datos que sera del tipo empresa
	 *
	 * @throws 	Exception
	 * 			if the generator function fails
	 */
	public static void generateFromFile(String inPathFile, String pathFile, int cantidadGenerar) throws Exception {
		File file = new File(Paths.get(pathFile).toAbsolutePath().toString());
		if (file.exists()) {
			file.delete();
		}
		file.createNewFile();
		
		BufferedWriter writer = Files.newBufferedWriter(Paths.get(pathFile));
		CSVPrinter csvPrinter = new CSVPrinter(writer,
				CSVFormat.INFORMIX_UNLOAD.withHeader("account_number","last_four_digits").withDelimiter(','));

		List<String> rows = FileUtils.getLinesFromResource(inPathFile, false);
		int catRows = 0;
		for (String row : rows) {
			
			String[] splited = row.split(",");
			csvPrinter.printRecord(splited[0],splited[1]);

			catRows++;
			if(catRows == cantidadGenerar) {
				break;
			}
		}
		
		csvPrinter.flush();
		csvPrinter.close();
	}
	
	public static void copyFromFile(String inPathFile, String pathFile, int cantidadGenerar) throws Exception {	
		FileUtils.copyFile(inPathFile,pathFile);
	}
	
	/**
	 * Creates a csv file used by the jmeter to create various types of credit account to stop debit
	 *
	 * @param 	pathFile
	 * 		  	to store the generated csv, relative to the project folder root location
	 * @param 	cantidadGenerar
	 * 		  	total number of data rows
	 * @param 	percentageBusiness
	 * 		  	Porcentaje de datos que sera del tipo empresa
	 *
	 * @throws 	Exception
	 * 			if the generator function fails
	 */
	public static void staticData(String pathFile, int cantidadGenerar) throws Exception {
		generateFromFile("staticData.csv", pathFile, cantidadGenerar);
	}
	
	public static void loginData(String pathFile, int cantidadGenerar) throws Exception {
		copyFromFile("UserData.csv", pathFile, cantidadGenerar);
	}
	
	public static void p2pData(String pathFile, int cantidadGenerar) throws Exception {
		generatep2pData(pathFile, cantidadGenerar);
	}
	
	public static void rapipagoData(String pathFile, int cantidadGenerar) throws Exception {
		String pathFileCellPhone = pathFile.substring(0,pathFile.indexOf('.')) + "_cellphone.csv";	
		copyFromFile("CellPhoneData.csv", pathFileCellPhone, cantidadGenerar);
		
		generateRapipagoData(pathFile, cantidadGenerar , pathFileCellPhone);
		
	}
	/**
	 * Creates a csv file used by the jmeter from query
	 *
	 * @param 	pathFile
	 * 		  	to store the generated csv, relative to the project folder root location
	 * @param 	cantidadGenerar
	 * 		  	total number of data rows
	 *
	 * @throws 	Exception
	 * 			if the generator function fails
	 */
	public static void generateFromQuery(String pathFile, int cantidadGenerar) throws Exception {
		cleanFile(pathFile);
		
		String queryKey = PropertyManager.getProperty("data.generate.query.key");
		
		String query = Queries.getQuery(queryKey);
		
		if(query.indexOf("TOP") != -1) {
			query = String.format(query, Integer.toString(cantidadGenerar), "1");
		} else {
			query = query.replace("SELECT", String.format(TOP_REPLACEMENT, Integer.toString(cantidadGenerar)));
		}
		
		List<Map<String, Object>> data = DatabaseUtils.executeQuery(query );
		
		BufferedWriter writer = Files.newBufferedWriter(Paths.get(pathFile));
		
		String[] headers = (String[]) data.get(0).keySet().toArray();

		CSVPrinter csvPrinter = new CSVPrinter(writer,
				CSVFormat.INFORMIX_UNLOAD.withHeader(headers).withDelimiter(','));
		
		for (Map<String, Object> person : data) {
			csvPrinter.printRecord(person.values());
		}
		
		csvPrinter.flush();
		csvPrinter.close();

	}
	
	private static void cleanFile(String pathFile) throws IOException {
		File file = new File(Paths.get(pathFile).toAbsolutePath().toString());
		if (file.exists()) {
			file.delete();
		}
		file.createNewFile();
	}

}